//
//  Created by Arnaud Blanchard on 22/12/15.
//  Copyright ETIS 2015. All rights reserved.
//

#include "serial.h"
#include "blc_core.h" //EXIT_ON..
#include "blc_channel.h"//blc_channel
#include "blc_program.h" //blc_program...
#include <errno.h> //errno
#include <unistd.h>
#include <stdio.h> //getline
#include <stdlib.h> //strtol

int mapping_ports(char const *string, int *ports, int ports_max){
	char const *current_pos=string;
	int port_id, i, read_chars_nb, result;

	FOR(i, ports_max){
		result=sscanf(current_pos, "%d%n", &port_id, &read_chars_nb);
		if (read_chars_nb==0) break;
		else if (result==0) EXIT_ON_ERROR("Wrong value '%s' for a port id", current_pos);
		else {
			if (port_id>ports_max) EXIT_ON_ERROR("Your port '%d' must be lower than '%d'", port_id, ports_max);
			ports[i] = port_id;
		}
		if (current_pos[0]==0) break;
		else current_pos+=read_chars_nb;
	}
	return i; //Number of mapped ports
}

int main(int argc, char **argv){
    blc_channel input;
    char const *period_str, *device_name, *input_name;
    int period;
    char buffer[256];
    char *input_line=NULL;
    size_t input_line_capablity=0;
    blc_mem sending_mem, receiving_mem;
    char const *servos;
    char const  *baudrate_str;
    int  fd;
    size_t i;
    int  baudrate;
    int motors_nb=32; //ssc32
    int  ports_map[32];

    blc_program_add_option(&baudrate_str, 'b', "baudrate", "integer",  "serial port speed", "115200");
    blc_program_add_option(&period_str, 'p', "period", "integer", "minimal period between two orders in ms", "0");
    blc_program_add_option(&servos, 's', "servos", "string",  "pins to drive servo motors \"pin1 pin2 pin3 ...\"", "0");
    blc_program_add_option(&device_name, 'D', "device", "string", "device to use for the sound", "/dev/ttyUSB0");
    blc_program_add_parameter(&input_name, "blc_channel-in", 1, "device name", NULL);
    blc_program_init(&argc, &argv, NULL);
    
    SYSTEM_SUCCESS_CHECK(sscanf(baudrate_str, "%d", &baudrate), 1, "parsing baudrate '%s'", baudrate_str);
    SYSTEM_SUCCESS_CHECK(sscanf(period_str, "%d", &period), 1, "parsing period '%s'", period_str);
    motors_nb=mapping_ports(servos, ports_map, 32);
    fprintf(stderr, "Mode mini SSC-II (compatible with SSC12, SSC32 and pololu)\n");

    fd = open_serial_port(device_name, baudrate, 8, 'N', 2);
    input.open(input_name, BLC_CHANNEL_READ);
    //Synchronize the loop with the input channel
    blc_loop_try_add_waiting_semaphore(input.sem_new_data);
    blc_loop_try_add_posting_semaphore(input.sem_ack_data);

    if (input.type!='UIN8') EXIT_ON_CHANNEL_ERROR(&input, "The input type must be 'UIN8' (unsigned char) but it is not:");
    else if (input.total_length!=motors_nb) EXIT_ON_CHANNEL_ERROR(&input, "The numbers of the motors '%d' does not fit the length of the input", motors_nb);

   BLC_COMMAND_LOOP(0){
    		//Format SSC2
		buffer[0]=255;
    		FOR(i, input.total_length){
    			buffer[1]=ports_map[i];
    			buffer[2]=input.uchars[i];
    			SYSTEM_SUCCESS_CHECK(write(fd, buffer, 3), 3,"Writing to the serial port");
    		}
    	}
    return EXIT_SUCCESS;
}
