#ifndef SERIAL_PORT_H
#define SERIAL_PORT_H

#include <stdlib.h>

#define EXIT_ON_SERIAL_PORT_ERROR(fd, ...) serial_port_fatal_error(fd, __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)


int open_serial_port(char const *device_name, int baudrate, int bits_nb,  char parity,  int stop_bits);
void eprint_serial_port_infos(int fd);
void serial_send_buffer(int fd, char *buffer, size_t size);
int serial_receive_buffer(int fd, char *buffer, size_t size, int timeout_ms);
void serial_port_fatal_error(int fd, const char *name_of_file, const char* name_of_function, int numero_of_line, const char *message, ...);


#endif
