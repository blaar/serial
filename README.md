Programs to communicate with serial devices
===========================================

serial_servos
============

Communicate with a ssc12|32 or pololu board to drive servos in mode Mini-SSC II.

Install
-------

You need to install  [blaar](https://blaar.org), it will be included. Then in the blaar directory:

usage
-----

    ./run.sh serial/serial_servos --baudrate=38400 --servos="1 5 7"
    

Communicate in **Mini SSC-II** mode (compatible SSC12, SSC32 and pololu) with **baudrate** in parameter waiting for space or tab separated values for **servos**  1, 5 and 7.
You can fill these values with the keyboard or with a **cat** and a file e.g motor_positions ( line starting with # are comments ):

    #motor1 motor5 motor7
    25      36     89

Then call :
    
    cat motor_positions | ./run.sh serial/serial_servos --baudrate=38400 --servos="1 5 7"

You can also give a file with multiline positions and add a **--period=1000** option. It will successively apply these positions with 1000ms interval.

Use **--help** for all options

arduino
=======

Communicate with an arduino board to acquire analog values, set servos positions, ... (in developmment)


More information about serial communication
===========================================

Good general information for unix programming: [Serial Programming Guide for POSIX Operating Systems](https://www.cmrr.umn.edu/~strupp/serial.html)

